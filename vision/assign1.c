#include<reg51.h>
sbit latch = P1^4;
unsigned char led[] = {0x01,0x02,0x04,0x06,0x10,0x20,0x40,0x80};		
unsigned char m = 0;

void delay(unsigned char tick){
	unsigned char i,j;
	for(i=0;i<tick;i++)
		for(j=0;j<20;j++);
}

void main(void){
	latch = 1;
	while(1){
		for(m=0;m<8;m++){
			P0=led[m];
			delay(20);
		}
	}
}